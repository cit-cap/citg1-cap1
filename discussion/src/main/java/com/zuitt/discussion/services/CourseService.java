package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    Iterable<Course> getCourses();

}
