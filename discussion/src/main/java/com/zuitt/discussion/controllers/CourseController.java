package com.zuitt.discussion.controllers;


import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course) {
        courseService.createCourse(stringToken, course);
        return new ResponseEntity<Object>("Course added successfully.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<Object>(courseService.getCourses(), HttpStatus.OK);
    }
}
